# Legacy Dashboard Package

This package assists in integration with reThink's Legacy Dashboard system by providing models, factories, seeders, and migrations for some of its resources.

## Setup

1. Add repository to project's `composer.json` file. (see below)
2. `composer require rethink-group/legacy-dashboard:dev-master`
3. `composer install`
4. Add the following line to the `provider` array in `config/app.php`: `RethinkLegacyDashboard\Providers\DatabaseProvider::class,`
5. `artisan vendor:publish --tag=legacy-dashboard` 
6. `php artisan migrate` (Migrations will be picked up from the `database/migrations` folder)

## Seeding

Two sets of seeders are provided. One with required data, such as reThink's organization record, and one that produces test data. For your purposes, include one of the following in your `seeds/DatabaseSeeder.php`'s `run` method:

For required data only:
```php
$this->call(\RethinkLegacyDashboard\RequiredDataSeeder::class);
```

For required **and** test data:
```php
$this->call(\RethinkLegacyDashboard\DatabaseSeeder::class);
```

### How to add repository
```
"repositories" : [
    {
        "type" : "vcs",
        "url" : "https://gitlab.com/therethinkgroupinc/packages/legacy-dashboard",
        "options": {
            "ssh2": {
                "pubkey_file": "~/.ssh/id_rsa.pub",
                "privkey_file": "~/.ssh/id_rsa"
            }
        }
    }
],
```