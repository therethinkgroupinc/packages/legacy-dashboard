<?php

namespace RethinkLegacyDashboard\Repositories;

use RethinkLegacyDashboard\Model\DashboardOrganization;
use RethinkLegacyDashboard\Contracts\Repositories\DashboardOrganizationRepository as Contract;

class DashboardOrganizationRepository implements Contract
{
    /** 
     * The model that defines an organization resource.
     * 
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /** 
     * Set the repository model
     */
    public function __construct()
    {
        $this->model = new DashboardOrganization;        
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return $this->model->get();
    }

    /**
     * {@inheritdoc}
     */
    public function find(int $id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * {@inheritdoc}
     */
    public function search(string $query)
    {
        $query = trim($query);

        return $this->model->where('acctName', 'like', "%$query%")
            ->orWhere('acctPhysAddress1', 'like', "%$query%")
            ->orWhere('acctMailAddress1', 'like', "%$query%")
            ->orWhere('acctAddressCleanAddress', 'like', "%$query%")
            ->orWhere('acctID', 'like', "%$query%")
            ->get();
    }
}
