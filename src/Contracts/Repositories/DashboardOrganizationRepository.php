<?php

namespace RethinkLegacyDashboard\Contracts\Repositories;

use RethinkLegacyDashboard\Model\DashboardOrganization;

interface DashboardOrganizationRepository
{
    /**
     * Get all organizations.
     *
     * @return Collection
     */
    public function all();

    /**
     * Get the organization with the given ID.
     *
     * @param  int  $id
     * @return \RethinkLegacyDashboard\Models\DashboardOrganization|null
     */
    public function find(int $id);

    /**
     * Find an organization by the given query
     *
     * @param  string $query
     * @return Collection
     */
    public function search(string $query);
}
