<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountDetailTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `accountDetail` (
          `rec` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `acctID` varchar(32) NOT NULL DEFAULT '',
          `name` varchar(32) NOT NULL DEFAULT '',
          `value` varchar(16) NOT NULL DEFAULT '',
          PRIMARY KEY (`rec`),
          UNIQUE KEY `acctNameValue` (`acctID`,`name`,`value`),
          KEY `rec` (`rec`),
          KEY `name` (`name`),
          KEY `acctID` (`acctID`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accountDetail');
    }
}
