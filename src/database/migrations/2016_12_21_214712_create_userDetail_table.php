<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `userDetail` (
          `userDetailRec` int(11) NOT NULL AUTO_INCREMENT,
          `userDetailUserID` varchar(64) NOT NULL DEFAULT '',
          `userDetailAcctID` varchar(32) NOT NULL DEFAULT '',
          `userDetailParentID` varchar(32) NOT NULL DEFAULT '',
          `userDetailType` varchar(32) NOT NULL DEFAULT '',
          `userDetailSubType` varchar(32) NOT NULL DEFAULT '',
          `userDetailValue` varchar(32) NOT NULL DEFAULT '',
          `userDetailDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (`userDetailRec`),
          UNIQUE KEY `userDetailAcctID` (`userDetailAcctID`,`userDetailUserID`,`userDetailType`,`userDetailSubType`),
          KEY `userDetailType` (`userDetailType`),
          KEY `userDetailSubType` (`userDetailSubType`),
          KEY `userDetailValue` (`userDetailValue`),
          KEY `userDetailUserID` (`userDetailUserID`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userDetail');
    }
}
