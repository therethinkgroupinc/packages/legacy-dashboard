<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingImageTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `billingImage` (
          `rec` int(11) NOT NULL AUTO_INCREMENT,
          `acctID` varchar(32) NOT NULL,
          `invoiceID` varchar(32) NOT NULL,
          `image` text NOT NULL,
          `dateTime` datetime NOT NULL,
          PRIMARY KEY (`rec`),
          KEY `acctID` (`acctID`,`invoiceID`,`dateTime`),
          KEY `invoiceID` (`invoiceID`),
          KEY `dateTime` (`dateTime`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billingImage');
    }
}
