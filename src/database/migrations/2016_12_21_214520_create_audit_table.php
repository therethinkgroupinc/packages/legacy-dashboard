<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `audit` (
          `auditRec` int(11) NOT NULL AUTO_INCREMENT,
          `auditAdminID` varchar(32) NOT NULL DEFAULT '',
          `auditAdminFirstName` varchar(16) NOT NULL DEFAULT '',
          `auditAdminLastName` varchar(16) NOT NULL DEFAULT '',
          `auditDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `auditAcctID` varchar(32) NOT NULL DEFAULT '',
          `auditUserID` varchar(32) NOT NULL DEFAULT '',
          `auditUserFirstName` varchar(16) NOT NULL DEFAULT '',
          `auditUserLastName` varchar(16) NOT NULL DEFAULT '',
          `auditProd` varchar(32) NOT NULL DEFAULT '',
          `auditFieldName` varchar(30) NOT NULL DEFAULT '',
          `auditNewData` text NOT NULL,
          `auditText` text NOT NULL,
          PRIMARY KEY (`auditRec`),
          KEY `auditAdminID` (`auditAdminID`),
          KEY `auditAcctID` (`auditAcctID`),
          KEY `auditDateTime` (`auditDateTime`),
          KEY `auditUserID` (`auditUserID`),
          KEY `auditProd` (`auditProd`),
          KEY `auditFieldName` (`auditFieldName`),
          KEY `auditAdminLastName` (`auditAdminLastName`),
          KEY `auditUserLastName` (`auditUserLastName`),
          KEY `auditAdminFirstName` (`auditAdminFirstName`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit');
    }
}
