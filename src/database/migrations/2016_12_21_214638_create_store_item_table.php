<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreItemTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `store_item` (
          `sitmRec` int(11) NOT NULL AUTO_INCREMENT,
          `sitmID` varchar(32) NOT NULL,
          `sitmProduct` varchar(32) NOT NULL,
          `sitmName` varchar(64) NOT NULL,
          `sitmDateAvailable` text NOT NULL,
          `sitmVendorPurchase` varchar(32) NOT NULL,
          `sitmVendorSupply` varchar(32) NOT NULL,
          `sitmCost` decimal(7,2) NOT NULL,
          `sitmLength` decimal(7,4) NOT NULL,
          `sitmWidth` decimal(7,4) NOT NULL,
          `sitmHeight` decimal(7,4) NOT NULL,
          `sitmWeight` decimal(7,3) NOT NULL,
          `sitmDimWeightIncrement` int(5) NOT NULL DEFAULT '1',
          `sitmMasterPackQty` int(11) NOT NULL,
          `sitmMasterPackType` varchar(1) NOT NULL,
          `sitmMasterPackLength` decimal(7,2) NOT NULL,
          `sitmMasterPackWidth` decimal(7,2) NOT NULL,
          `sitmMasterPackHeight` decimal(7,2) NOT NULL,
          `sitmMasterPackWeight` decimal(7,2) NOT NULL,
          `sitmCustomBoxID` int(1) NOT NULL,
          `sitmSequence` decimal(11,6) NOT NULL,
          PRIMARY KEY (`sitmRec`),
          UNIQUE KEY `productId` (`sitmProduct`,`sitmID`),
          UNIQUE KEY `sitmProduct` (`sitmProduct`,`sitmSequence`),
          KEY `vendorPurchase` (`sitmVendorPurchase`),
          KEY `vendorSupply` (`sitmVendorSupply`),
          KEY `sitmSequence` (`sitmSequence`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_item');
    }
}
