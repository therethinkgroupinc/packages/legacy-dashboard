<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `billing` (
          `billRec` int(11) NOT NULL AUTO_INCREMENT,
          `billInvoiceID` varchar(32) NOT NULL DEFAULT '',
          `billPurchaseOrder` varchar(32) NOT NULL,
          `billAcctID` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
          `billUserID` varchar(32) NOT NULL DEFAULT '',
          `billPayType` varchar(16) NOT NULL DEFAULT '',
          `billProd` varchar(32) NOT NULL DEFAULT '',
          `billProdDetail` varchar(32) NOT NULL DEFAULT '',
          `billPSCount` varchar(8) NOT NULL DEFAULT '',
          `billK5Count` varchar(8) NOT NULL DEFAULT '',
          `billChildCount` varchar(8) NOT NULL DEFAULT '',
          `billProdStartDate` date NOT NULL,
          `billLicenseTerm` int(4) NOT NULL DEFAULT '0',
          `billMaxUsers` int(4) NOT NULL DEFAULT '0',
          `billNewRenew` varchar(16) NOT NULL DEFAULT '',
          `billEmail` varchar(64) NOT NULL DEFAULT '',
          `billCardFirstName` varchar(32) NOT NULL DEFAULT '',
          `billCardLastName` varchar(32) NOT NULL DEFAULT '',
          `billCardType` varchar(32) NOT NULL DEFAULT '',
          `billCardNumber` varchar(20) NOT NULL DEFAULT '',
          `billCardExpireMonth` int(4) NOT NULL DEFAULT '0',
          `billCardExpireYear` int(8) NOT NULL DEFAULT '0',
          `billCardAddress1` varchar(32) NOT NULL DEFAULT '',
          `billCardCity` varchar(32) NOT NULL DEFAULT '',
          `billCardState` varchar(64) NOT NULL DEFAULT '',
          `billCardZip` varchar(20) NOT NULL DEFAULT '',
          `billCardCountry` varchar(32) NOT NULL DEFAULT '',
          `billCardPhone` varchar(50) NOT NULL DEFAULT '',
          `billCheckABA` varchar(10) NOT NULL DEFAULT '0',
          `billCheckAcctNumber` varchar(32) NOT NULL DEFAULT '0',
          `billCheckAcctType` varchar(16) NOT NULL DEFAULT '',
          `billCheckBankName` varchar(32) NOT NULL DEFAULT '',
          `billCheckAcctHolder` varchar(64) NOT NULL DEFAULT '',
          `billCreateDate` date NOT NULL,
          `billPurchaseDate` date NOT NULL,
          `billTotal` varchar(10) NOT NULL DEFAULT '',
          `billCoupID` varchar(16) NOT NULL DEFAULT '',
          `billTransactionID` varchar(64) NOT NULL DEFAULT '',
          `billResponseCode` varchar(64) NOT NULL DEFAULT '',
          `billAuthorizationCode` varchar(64) NOT NULL DEFAULT '',
          `billStatus` varchar(32) NOT NULL DEFAULT '',
          `billAffCampID` varchar(16) NOT NULL DEFAULT '',
          `billShipToID` char(32) NOT NULL DEFAULT '',
          `billCCID` char(32) NOT NULL DEFAULT '',
          PRIMARY KEY (`billRec`),
          KEY `billInvoiceID` (`billInvoiceID`),
          KEY `billUserID` (`billUserID`),
          KEY `billProdDetail` (`billProdDetail`),
          KEY `billAcctID` (`billAcctID`),
          KEY `billAffCampID` (`billAffCampID`),
          KEY `billPurchaseOrder` (`billPurchaseOrder`),
          KEY `billShipToID` (`billShipToID`),
          KEY `billCCID` (`billCCID`),
          KEY `billStatus` (`billStatus`),
          KEY `billPurchaseDate` (`billPurchaseDate`),
          KEY `billProd` (`billProd`),
          KEY `billNewRenew` (`billNewRenew`),
          KEY `billCoupID` (`billCoupID`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing');
    }
}
