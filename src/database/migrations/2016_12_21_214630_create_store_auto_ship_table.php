<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreAutoShipTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `store_auto_ship` (
          `sasRec` int(11) NOT NULL AUTO_INCREMENT,
          `sasAutoShipID` varchar(32) NOT NULL,
          `sasOrderQueueID` varchar(32) NOT NULL,
          `sasSoqlLineNumber` int(11) NOT NULL DEFAULT '0',
          `sasUserID` varchar(32) NOT NULL,
          `sasAcctID` varchar(32) NOT NULL,
          `sasProdID` varchar(32) NOT NULL,
          `sasQty` int(11) NOT NULL,
          `sasCCID` varchar(32) NOT NULL,
          `sasShipToID` varchar(32) NOT NULL,
          `sasShipMethodID` varchar(16) NOT NULL DEFAULT '',
          `sasDateStart` date NOT NULL,
          `sasDateTimeCancel` datetime NOT NULL,
          `sasDateTimeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `sasDetails` text NOT NULL,
          `sasPriceTotal` decimal(7,2) NOT NULL,
          `sasAutoCCFail1` datetime NOT NULL,
          `sasAutoCCFail2` datetime NOT NULL,
          `sasAutoCCFailResume` date NOT NULL,
          `sasShipToFail` datetime NOT NULL,
          `sasPriceTotalFixed` decimal(7,2) NOT NULL,
          `sasPriceTotalIsFixed` tinyint(1) NOT NULL DEFAULT '0',
          `sasShippingTotalFixed` decimal(7,2) NOT NULL,
          `sasShippingTotalIsFixed` tinyint(4) NOT NULL DEFAULT '0',
          `sasItemID` varchar(32) NOT NULL,
          PRIMARY KEY (`sasRec`),
          UNIQUE KEY `sasAutoShipID` (`sasAutoShipID`),
          KEY `sasProductID` (`sasProdID`),
          KEY `sasCCID` (`sasCCID`),
          KEY `sasShipToID` (`sasShipToID`),
          KEY `sasDateStart` (`sasDateStart`),
          KEY `sasDateCancel` (`sasDateTimeCancel`),
          KEY `sasUserID` (`sasUserID`),
          KEY `sasAcctID` (`sasAcctID`),
          KEY `sasDateTimeCreated` (`sasDateTimeCreated`),
          KEY `sasAutoCCFail1` (`sasAutoCCFail1`),
          KEY `sasAutoCCFail2` (`sasAutoCCFail2`),
          KEY `sasShipToFail` (`sasShipToFail`),
          KEY `sasShippingTotalIsFixed` (`sasShippingTotalIsFixed`),
          KEY `sasPriceTotalIsFixed` (`sasPriceTotalIsFixed`),
          KEY `sasAutoCCFailResume` (`sasAutoCCFailResume`),
          KEY `sasItemID` (`sasItemID`),
          KEY `sasOrderQueueID` (`sasOrderQueueID`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_auto_ship');
    }
}
