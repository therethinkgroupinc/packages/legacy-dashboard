<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `accounts` (
          `acctRec` int(11) NOT NULL AUTO_INCREMENT,
          `acctID` varchar(32) NOT NULL DEFAULT '',
          `acctActive` tinyint(4) NOT NULL DEFAULT '0',
          `acctType` varchar(20) NOT NULL DEFAULT '',
          `acctSubType` varchar(40) NOT NULL DEFAULT '',
          `acctPrincipal` varchar(64) NOT NULL DEFAULT '',
          `acctPrincipalJobTitle` varchar(64) NOT NULL DEFAULT '',
          `acctSmallGroupCoach` varchar(64) NOT NULL DEFAULT '',
          `acctPreschoolDirector` varchar(32) NOT NULL DEFAULT '',
          `acctPreschoolDirectorEmail` varchar(64) NOT NULL DEFAULT '',
          `acctChildrenDirector` varchar(32) NOT NULL DEFAULT '',
          `acctChildrenDirectorEmail` varchar(64) NOT NULL DEFAULT '',
          `acctStudentDirector` varchar(32) NOT NULL DEFAULT '',
          `acctStudentDirectorEmail` varchar(64) NOT NULL DEFAULT '',
          `acctFamilyDirector` varchar(32) NOT NULL DEFAULT '',
          `acctFamilyDirectorEmail` varchar(64) NOT NULL DEFAULT '',
          `acctName` varchar(64) NOT NULL DEFAULT '',
          `acctPhysAddress1` varchar(64) NOT NULL DEFAULT '',
          `acctPhysAddress2` varchar(64) NOT NULL DEFAULT '',
          `acctPhysCity` varchar(64) NOT NULL DEFAULT '',
          `acctPhysState` varchar(32) NOT NULL DEFAULT '',
          `acctPhysZip` varchar(20) NOT NULL DEFAULT '',
          `acctCountry` varchar(32) NOT NULL DEFAULT '',
          `acctLatitude` float NOT NULL,
          `acctLongitude` float NOT NULL,
          `acctMapDisplay` int(4) NOT NULL DEFAULT '1',
          `acctMailAddress1` varchar(64) NOT NULL DEFAULT '',
          `acctMailAddress2` varchar(64) NOT NULL DEFAULT '',
          `acctMailCity` varchar(64) NOT NULL DEFAULT '',
          `acctMailState` varchar(32) NOT NULL DEFAULT '',
          `acctMailZip` varchar(20) NOT NULL DEFAULT '',
          `acctPhone` varchar(15) NOT NULL DEFAULT '',
          `acctFax` varchar(15) NOT NULL DEFAULT '',
          `acctYearEst` int(4) NOT NULL DEFAULT '0',
          `acctCreateDate` date NOT NULL DEFAULT '1900-01-01',
          `acctMemberCount` int(11) NOT NULL DEFAULT '0',
          `acctRev` tinyint(4) NOT NULL DEFAULT '1',
          `acctSnapShot` varchar(16) NOT NULL DEFAULT '',
          `acctParent` varchar(32) NOT NULL,
          `acctTestLevel` int(11) NOT NULL DEFAULT '0',
          `acctOrangeLevel` tinyint(4) NOT NULL DEFAULT '0',
          `acctComplete` int(4) NOT NULL DEFAULT '1',
          `acctTopInfluencers` tinyint(1) NOT NULL DEFAULT '0',
          `acctDenomination` char(40) NOT NULL,
          `acctNetworkAffiliation` char(40) NOT NULL DEFAULT '',
          `acctNetworkAffiliationOther` char(40) NOT NULL,
          `acctEnvironment` text NOT NULL,
          `acctRegularAttendance` int(4) NOT NULL,
          `acctPreschoolMinistrySize` int(4) NOT NULL,
          `acctElementaryMinistrySize` int(4) NOT NULL,
          `acctMiddleSchoolMinistrySize` int(4) NOT NULL,
          `acctHighSchoolMinistrySize` int(4) NOT NULL,
          `acctAddressCleanTimeStamp` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
          `acctAddressCleanAddress` varchar(64) NOT NULL DEFAULT '',
          `acctAddressCleanCity` varchar(64) NOT NULL DEFAULT '',
          `acctAddressCleanState` varchar(32) NOT NULL DEFAULT '',
          `acctAddressCleanZip` varchar(20) NOT NULL DEFAULT '',
          `acctAddressCleanCountry` varchar(32) NOT NULL DEFAULT '',
          `acctAddressCleanAttemptTimeStamp` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
          `acctAddressCleanAttemptError` varchar(40) NOT NULL DEFAULT '',
          `acctAddressCleanAttemptMessage` text NOT NULL,
          `acctOBR_organization_id` int(11) NOT NULL,
          `acctOBR_organization_created_at` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
          PRIMARY KEY (`acctRec`),
          UNIQUE KEY `acctID` (`acctID`),
          KEY `acctActive` (`acctActive`),
          KEY `acctType` (`acctType`),
          KEY `acctSubType` (`acctSubType`),
          KEY `acctName` (`acctName`),
          KEY `acctPhysAddress1` (`acctPhysAddress1`),
          KEY `acctPhysCity` (`acctPhysCity`),
          KEY `acctPhysState` (`acctPhysState`),
          KEY `acctPhone` (`acctPhone`),
          KEY `acctParent` (`acctParent`),
          KEY `acctOrangeLevel` (`acctOrangeLevel`),
          KEY `acctLatitude` (`acctLatitude`,`acctLongitude`),
          KEY `acctTestLevel` (`acctTestLevel`),
          KEY `acctTopInfluencers` (`acctTopInfluencers`),
          KEY `acctName_2` (`acctName`,`acctCountry`,`acctPhysState`,`acctPhysCity`),
          KEY `acctAddressCleanTimeStamp` (`acctAddressCleanTimeStamp`),
          KEY `acctAddressCleanState` (`acctAddressCleanState`),
          KEY `acctAddressCleanCountry` (`acctAddressCleanCountry`)
        ) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
