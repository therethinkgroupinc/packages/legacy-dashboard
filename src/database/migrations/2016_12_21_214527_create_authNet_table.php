<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthNetTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `authNet` (
          `authNetRec` int(11) NOT NULL AUTO_INCREMENT,
          `authNetAcctID` varchar(32) NOT NULL DEFAULT '',
          `authNetUserID` varchar(32) NOT NULL DEFAULT '',
          `authNetDescription` text NOT NULL,
          `authNetAmount` decimal(11,2) NOT NULL DEFAULT '0.00',
          `authNetResponseCode` int(2) NOT NULL DEFAULT '0',
          `authNetReasonCode` varchar(4) NOT NULL DEFAULT '',
          `authNetAVSCode` varchar(4) NOT NULL DEFAULT '',
          `authNetReasonText` varchar(128) NOT NULL,
          `authNetBrowserInfo` text NOT NULL,
          `authNetDateTime` datetime NOT NULL,
          `authNetSendInfo` text NOT NULL,
          `authNetReturnInfo` text NOT NULL,
          `authNetResponseInfo` text NOT NULL,
          `authNetPaymentKey` varchar(32) NOT NULL DEFAULT '',
          `authNetUniqueID` varchar(32) NOT NULL DEFAULT '',
          `authNetTax` decimal(11,2) NOT NULL DEFAULT '0.00',
          `authNetFreight` decimal(11,2) NOT NULL DEFAULT '0.00',
          PRIMARY KEY (`authNetRec`),
          KEY `authNetAcctID` (`authNetAcctID`,`authNetUserID`),
          KEY `authNetUserID` (`authNetUserID`),
          KEY `authNetDateTime` (`authNetDateTime`),
          KEY `authNetUniqueID` (`authNetUniqueID`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authNet');
    }
}
