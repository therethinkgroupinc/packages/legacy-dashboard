<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBadWordsTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `bad_words` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `word` varchar(16) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bad_words');
    }
}
