<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreOrderTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `store_order` (
          `soRec` int(11) NOT NULL AUTO_INCREMENT,
          `soOrderID` varchar(32) NOT NULL,
          `soOrderQueueID` varchar(32) NOT NULL,
          `soAcctID` varchar(32) NOT NULL,
          `soUserID` varchar(32) NOT NULL,
          `soCCID` varchar(32) NOT NULL,
          `soShipToID` varchar(32) NOT NULL,
          `soShipMethodID` varchar(16) NOT NULL,
          `soDateTimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `soSalesTaxGA` decimal(7,2) NOT NULL DEFAULT '0.00',
          `soShipCost` decimal(7,2) NOT NULL DEFAULT '0.00',
          PRIMARY KEY (`soRec`),
          UNIQUE KEY `soqueueOrderID` (`soOrderID`),
          KEY `soqueueAcctID` (`soAcctID`),
          KEY `soqueueUserID` (`soUserID`),
          KEY `soqueueCCID` (`soCCID`),
          KEY `soqueueShipToID` (`soShipToID`),
          KEY `soqueueShipMethodID` (`soShipMethodID`),
          KEY `soqueueDateTimeStamp` (`soDateTimeStamp`),
          KEY `soOrderQueueID` (`soOrderQueueID`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_order');
    }
}
