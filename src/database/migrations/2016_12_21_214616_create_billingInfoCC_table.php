<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingInfoCCTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `billingInfoCC` (
          `billInfoCCRec` int(11) NOT NULL AUTO_INCREMENT,
          `billInfoCCActive` int(1) NOT NULL DEFAULT '1',
          `billInfoCCAcctID` char(32) NOT NULL DEFAULT '',
          `billInfoCCID` char(32) NOT NULL DEFAULT '',
          `billInfoCCUserID` char(32) NOT NULL DEFAULT '',
          `billInfoCCFirstName` char(32) NOT NULL DEFAULT '',
          `billInfoCCLastName` char(32) NOT NULL DEFAULT '',
          `billInfoCCAddress` char(32) NOT NULL DEFAULT '',
          `billInfoCCCity` char(32) NOT NULL DEFAULT '',
          `billInfoCCState` char(2) NOT NULL DEFAULT '',
          `billInfoCCZip` char(32) NOT NULL DEFAULT '',
          `billInfoCCCountry` char(32) NOT NULL DEFAULT '',
          `billInfoCCType` char(8) NOT NULL DEFAULT '',
          `billInfoCCNumberE` tinytext NOT NULL,
          `billInfoCCCVVE` tinytext NOT NULL,
          `billInfoCCYearMonthE` varchar(32) NOT NULL DEFAULT '',
          `billInfoCCPreferred` binary(1) NOT NULL DEFAULT '0',
          `billInfoCCFromFamilyTimes` varchar(32) NOT NULL DEFAULT '',
          `billInfoCCNumberB` tinytext NOT NULL COMMENT 'CIM Customer Profile ID.',
          `billInfoCCCVVB` tinytext NOT NULL COMMENT 'CIM Payment Profile ID',
          `billInfoCCYearMonthB` blob NOT NULL,
          `billInfoCCNumberU` varchar(32) NOT NULL DEFAULT '',
          `billInfoCCCVVU` varchar(8) NOT NULL DEFAULT '',
          `billInfoCCYearMonthU` varchar(16) NOT NULL DEFAULT '',
          `billInfoCCNumberC` tinytext NOT NULL,
          `billInfoCCCVVC` tinytext NOT NULL,
          `billInfoCCTimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `billInfoCCSource` char(32) NOT NULL DEFAULT '',
          PRIMARY KEY (`billInfoCCRec`),
          UNIQUE KEY `billInfoCCID` (`billInfoCCID`),
          KEY `billInfoCCLastName` (`billInfoCCLastName`),
          KEY `billInfoCCState` (`billInfoCCState`),
          KEY `billInfoCCZip` (`billInfoCCZip`),
          KEY `billInfoCCPreferred` (`billInfoCCPreferred`),
          KEY `billInfoCCAcctID` (`billInfoCCAcctID`),
          KEY `billInfoCCTimeStamp` (`billInfoCCTimeStamp`),
          KEY `billInfoCCSource` (`billInfoCCSource`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billingInfoCC');
    }
}
