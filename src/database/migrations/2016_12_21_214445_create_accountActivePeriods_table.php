<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountActivePeriodsTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `accountActivePeriods` (
          `activeRec` int(11) NOT NULL AUTO_INCREMENT,
          `activeAcctID` varchar(32) NOT NULL DEFAULT '',
          `activeProd` varchar(32) NOT NULL DEFAULT '',
          `activeProdDetail` varchar(8) NOT NULL,
          `activeStartDate` date NOT NULL,
          `activeRenewalDate` date NOT NULL,
          `activeUsers` int(11) NOT NULL DEFAULT '0',
          `activeChildCount` int(8) NOT NULL DEFAULT '0',
          `activeHowHear` varchar(64) NOT NULL,
          `activeType` varchar(16) NOT NULL DEFAULT '',
          `activeTrial` int(1) NOT NULL DEFAULT '0',
          `activeTicketFlag` varchar(1) NOT NULL DEFAULT '',
          `activeTicket30DaysToExpireTicket` tinyint(1) NOT NULL DEFAULT '0',
          `activeTimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `activeDetails` text NOT NULL,
          `activePromoCode` varchar(32) NOT NULL,
          PRIMARY KEY (`activeRec`),
          KEY `activeAcctID` (`activeAcctID`),
          KEY `activeProd` (`activeProd`),
          KEY `activeStartDate` (`activeStartDate`),
          KEY `activeRenewalDate` (`activeRenewalDate`),
          KEY `activeProdDetail` (`activeProdDetail`),
          KEY `activeHowHear` (`activeHowHear`),
          KEY `activeTicketFlag` (`activeTicketFlag`),
          KEY `activeTimeStamp` (`activeTimeStamp`),
          KEY `activePromoCode` (`activePromoCode`),
          KEY `activeTicket30DaysToExpire` (`activeTicket30DaysToExpireTicket`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accountActivePeriods');
    }
}
