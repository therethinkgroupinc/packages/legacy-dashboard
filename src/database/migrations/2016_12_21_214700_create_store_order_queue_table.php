<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreOrderQueueTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `store_order_queue` (
          `soqRec` int(11) NOT NULL AUTO_INCREMENT,
          `soqOrderQueueID` varchar(32) NOT NULL,
          `soqAcctID` varchar(32) NOT NULL,
          `soqUserID` varchar(32) NOT NULL,
          `soqCCID` varchar(32) NOT NULL,
          `soqShipToID` varchar(32) NOT NULL,
          `soqShipMethodID` varchar(16) NOT NULL,
          `soqShipCost` decimal(7,2) NOT NULL DEFAULT '0.00',
          `soqSalesTaxGA` decimal(7,2) NOT NULL DEFAULT '0.00',
          `soqDateTimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `soqDateTimeProcessed` datetime NOT NULL,
          PRIMARY KEY (`soqRec`),
          UNIQUE KEY `soqueueOrderID` (`soqOrderQueueID`),
          KEY `soqueueAcctID` (`soqAcctID`),
          KEY `soqueueUserID` (`soqUserID`),
          KEY `soqueueCCID` (`soqCCID`),
          KEY `soqueueShipToID` (`soqShipToID`),
          KEY `soqueueShipMethodID` (`soqShipMethodID`),
          KEY `soqueueDateTimeStamp` (`soqDateTimeStamp`),
          KEY `soqDateTimeProcessed` (`soqDateTimeProcessed`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_order_queue');
    }
}
