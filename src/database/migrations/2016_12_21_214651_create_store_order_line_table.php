<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreOrderLineTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @TODO Refactor this into a legitimate Laravel migration.
     * @return void
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `store_order_line` (
          `solRec` int(11) NOT NULL AUTO_INCREMENT,
          `solOrderID` varchar(32) NOT NULL,
          `solLineNumber` int(11) NOT NULL,
          `solAutoShipID` varchar(32) NOT NULL,
          `solProdID` varchar(32) NOT NULL,
          `solItemID` varchar(32) NOT NULL,
          `solQty` int(11) NOT NULL,
          `solPricingPlan` varchar(1) NOT NULL,
          `solPriceTotal` decimal(7,2) NOT NULL,
          `solDetails` text NOT NULL,
          `solPromoCode` varchar(32) NOT NULL,
          `solUserCredit` varchar(32) NOT NULL DEFAULT '',
          `solDateStart` date NOT NULL,
          `solDateTimeProcessed` datetime NOT NULL,
          `solReceiptID` varchar(32) NOT NULL,
          `solDateTimeStatus` datetime NOT NULL,
          `solStatus` varchar(16) NOT NULL DEFAULT 'processed',
          `solDateTimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`solRec`),
          UNIQUE KEY `key` (`solOrderID`,`solLineNumber`),
          KEY `soqlProdID` (`solProdID`),
          KEY `soqlPricingPlan` (`solPricingPlan`),
          KEY `soqlPromoCode` (`solPromoCode`),
          KEY `soqlDateStart` (`solDateStart`),
          KEY `soqlDateTimeProcessed` (`solDateTimeProcessed`),
          KEY `solAutoShipID` (`solAutoShipID`),
          KEY `solDateTimeStatus` (`solDateTimeStatus`),
          KEY `solStatus` (`solStatus`),
          KEY `solUserCredit` (`solUserCredit`),
          KEY `solItemID` (`solItemID`),
          KEY `solReceiptID` (`solReceiptID`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_order_line');
    }
}
