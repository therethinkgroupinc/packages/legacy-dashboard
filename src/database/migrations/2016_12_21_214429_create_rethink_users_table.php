<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class CreateRethinkUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @TODO Refactor this into a legitimate Laravel migration.
     */
    public function up()
    {
        $query = "CREATE TABLE IF NOT EXISTS `users` (
          `userRec` int(11) NOT NULL AUTO_INCREMENT,
          `userID` varchar(32) NOT NULL DEFAULT '',
          `userActive` tinyint(4) NOT NULL DEFAULT '0',
          `userAcctID` varchar(32) NOT NULL DEFAULT '',
          `userFirstName` varchar(16) NOT NULL DEFAULT '',
          `userLastName` varchar(16) NOT NULL DEFAULT '',
          `userJobTitle` varchar(64) NOT NULL DEFAULT '',
          `userPhone` varchar(16) NOT NULL DEFAULT '',
          `userLevelSuper` varchar(8) NOT NULL DEFAULT '' COMMENT 'obsolete 11/18/08',
          `userFinancialSuper` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'obsolete 11/18/08',
          `userTickForwardID` varchar(32) NOT NULL DEFAULT '',
          `userLevel252B` varchar(8) NOT NULL DEFAULT '',
          `userParent252B` varchar(32) NOT NULL DEFAULT '',
          `userLevel252BS` varchar(8) NOT NULL DEFAULT '',
          `userParent252BS` varchar(32) NOT NULL DEFAULT '',
          `userLevelFiLo` varchar(8) NOT NULL DEFAULT '',
          `userParentFiLo` varchar(32) NOT NULL DEFAULT '',
          `userLevelFiLoS` varchar(8) NOT NULL DEFAULT '',
          `userParentFiLoS` varchar(32) NOT NULL DEFAULT '',
          `userLevelXP3` varchar(8) NOT NULL DEFAULT '',
          `userParentXP3` varchar(32) NOT NULL DEFAULT '',
          `userLevelXP3C` varchar(8) NOT NULL,
          `userParentXP3C` varchar(32) NOT NULL,
          `userLevelOL` varchar(8) NOT NULL DEFAULT '',
          `userParentOL` varchar(32) NOT NULL DEFAULT '',
          `userCreateDate` date NOT NULL DEFAULT '1900-01-01',
          `userLogin` varchar(64) NOT NULL DEFAULT '',
          `userPass` varchar(32) NOT NULL DEFAULT '',
          `userEmail` varchar(64) NOT NULL DEFAULT '',
          `userEmailPrefs` varchar(16) NOT NULL DEFAULT '1111111111111111',
          `userSurvey` varchar(16) NOT NULL DEFAULT '',
          `userPrefs` varchar(16) NOT NULL DEFAULT '0',
          `userContact` char(1) NOT NULL DEFAULT '',
          `userFromFamilyTimes` varchar(32) NOT NULL DEFAULT '',
          `userOBR_user_id` int(11) NOT NULL,
          `userOBR_user_created_at` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
          `userOBR_conversion_skip` tinyint(4) NOT NULL DEFAULT '0',
          PRIMARY KEY (`userRec`),
          UNIQUE KEY `userID` (`userID`),
          KEY `userAcctID` (`userAcctID`),
          KEY `userLogin` (`userLogin`),
          KEY `userParent252B` (`userParent252B`),
          KEY `userParentFiLo` (`userParentFiLo`),
          KEY `userTickForwardID` (`userTickForwardID`),
          KEY `userLastName` (`userLastName`),
          KEY `userEmail` (`userEmail`),
          KEY `userParentOL` (`userParentOL`),
          KEY `userLevel252B` (`userLevel252B`),
          KEY `userLevel252BS` (`userLevel252BS`),
          KEY `userLevelFiLo` (`userLevelFiLo`),
          KEY `userLevelFiLoS` (`userLevelFiLoS`),
          KEY `userLevelXP3` (`userLevelXP3`),
          KEY `userLevelOL` (`userLevelOL`),
          KEY `userLevelXP3C` (`userLevelXP3C`,`userParentXP3C`),
          KEY `userOBR_conversion_skip` (`userOBR_conversion_skip`)
        ) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;";

        \DB::statement($query);
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
