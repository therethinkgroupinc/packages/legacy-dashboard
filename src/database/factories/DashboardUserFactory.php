<?php
$factory->define(RethinkLegacyDashboard\Model\DashboardUser::class, function (\Faker\Generator $faker) {
    static $password;

    $gender = collect(['male', 'female'])->random();
    $email = $faker->email();

    return [
            'userID' => 'SEEDUSER'.strtoupper(str_random(8)),
            'userActive' => 1,
            'userAcctID' => '',
            'userFirstName' => $faker->firstName($gender),
            'userLastName' => $faker->lastName(),
            'userJobTitle' => collect([str_limit($faker->jobTitle(), 64, ''), ''])->random(),
            'userPhone' => collect([$faker->e164PhoneNumber(), ''])->random(),
            'userCreateDate' => $faker->dateTimeBetween('2002-01-01', 'now'),
            'userLogin' => collect([$faker->userName(), $email])->random(),
            'userPass' => collect([$faker->password(), str_random(rand(5,15 ))])->random(),
            'userEmail' => $email,
            'userEmailPrefs' => '1111111111111111',
            'userSurvey' => collect(['activate', ''])->random(),
            'userPrefs' => 0,
            'userOBR_user_id' => 0,
            'userLevelXP3C' => '',
            'userParentXP3C' => '',
        ];
});
