<?php

namespace RethinkLegacyDashboard;

use Illuminate\Database\Seeder;
use RethinkLegacyDashboard\Model\DashboardUser;
use RethinkLegacyDashboard\Model\DashboardOrganization;

class OrganizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Create a test user
        factory(DashboardUser::class)->create([
            'userAcctID' => 'ACCT27695401',
            'userFirstName' => 'Testing',
            'userLastName' => 'User',
            'userEmail' => 'test@example.com',
            'userLogin' => 'test@example.com',
            'userPass' => 'test123'
        ]);
        
        factory(DashboardOrganization::class, rand(50, 150))->create()->each(function ($o) {
            factory(DashboardUser::class, rand(1, 15))->create([
                 'userAcctID' => $o->acctID,
                 ]);
        });
    }
}
