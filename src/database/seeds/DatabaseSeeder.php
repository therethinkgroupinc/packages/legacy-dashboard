<?php
namespace RethinkLegacyDashboard;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RequiredDataSeeder::class);
        $this->call(TestingSeeder::class);
    }

    protected function truncateTables(Array $tables)
    {
        foreach ($tables as $table) {
            \DB::table($table)->truncate();
        } 
    }
}
