<?php

namespace RethinkLegacyDashboard;

class RequiredDataSeeder extends DatabaseSeeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $this->truncateTables(['accounts', 'users']);

        $this->call(BadWordsTableSeeder::class);
        $this->seedOrganizationsTable();
    }

    /**
     * @todo Refactor this by using the Organization factory
     * and overriding the attributes
     */
    protected function seedOrganizationsTable()
    {
        $existing = Model\DashboardOrganization::where('acctID', RETHINK_ORG_ID)->first();

        if (is_null($existing)) {
            \DB::statement(
                "INSERT INTO `accounts` (`acctID`, `acctActive`, `acctType`, `acctSubType`, `acctPrincipal`, `acctPrincipalJobTitle`, `acctSmallGroupCoach`, `acctPreschoolDirector`, `acctPreschoolDirectorEmail`, `acctChildrenDirector`, `acctChildrenDirectorEmail`, `acctStudentDirector`, `acctStudentDirectorEmail`, `acctFamilyDirector`, `acctFamilyDirectorEmail`, `acctName`, `acctPhysAddress1`, `acctPhysAddress2`, `acctPhysCity`, `acctPhysState`, `acctPhysZip`, `acctCountry`, `acctLatitude`, `acctLongitude`, `acctMapDisplay`, `acctMailAddress1`, `acctMailAddress2`, `acctMailCity`, `acctMailState`, `acctMailZip`, `acctPhone`, `acctFax`, `acctYearEst`, `acctCreateDate`, `acctMemberCount`, `acctRev`, `acctSnapShot`, `acctParent`, `acctTestLevel`, `acctOrangeLevel`, `acctComplete`, `acctTopInfluencers`, `acctDenomination`, `acctNetworkAffiliation`, `acctNetworkAffiliationOther`, `acctEnvironment`, `acctRegularAttendance`, `acctPreschoolMinistrySize`, `acctElementaryMinistrySize`, `acctMiddleSchoolMinistrySize`, `acctHighSchoolMinistrySize`, `acctAddressCleanTimeStamp`, `acctAddressCleanAddress`, `acctAddressCleanCity`, `acctAddressCleanState`, `acctAddressCleanZip`, `acctAddressCleanCountry`, `acctAddressCleanAttemptTimeStamp`, `acctAddressCleanAttemptError`, `acctAddressCleanAttemptMessage`, `acctOBR_organization_id`, `acctOBR_organization_created_at`)
				VALUES
					('".RETHINK_ORG_ID."', 1, 'Religious', 'Non-denominational', 'Reggie Joiner', 'EIC', '', '', '', '', '', '', '', '', '', 'The reThink Group', '5870 CHARLOTTE LN STE 300', '', 'CUMMING', 'GA', '30040-0321', 'US', 34.1837, -84.2186, 1, '5870 Charlotte Ave', 'Suite 300', 'Cumming', 'GA', '30040', '7702905600', '', 1998, '2004-06-08', 19000, 0, 'S10EBEBC', 'primary', 8, 0, 1, 0, 'American Baptist', '1', '', 'null', 676, 0, 0, 0, 0, '2016-06-10 14:40:57', '5870 CHARLOTTE LN STE 300', 'CUMMING', 'GA', '30040-0321', 'US', '1900-01-01 00:00:00', '', '', 110, '2016-10-25 11:13:00');"
            );
        }
    }
}
