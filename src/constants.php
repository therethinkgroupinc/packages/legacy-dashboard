<?php
/*
    Codes
 */
const CODE_PRIMARY_KEY = 'id';
const CODE_TABLE_NAME = 'ls_codes';

/*
    Licenses
 */
const LICENSE_PRIMARY_KEY = 'activeRec';
const LICENSE_TABLE_NAME = 'accountActivePeriods';
const LICENSE_RENEWAL = 'activeRenewalDate';
const LICENSE_START = 'activeStartDate';
const LICENSE_CODE = 'activeProdDetail';
const LICENSE_PRODUCT = 'activeProd';

/*
    Organizations
 */
const ORG_PRIMARY_KEY = 'acctRec';
const ORG_TABLE_NAME = 'accounts';
const ORG_DASHBOARD_ID = 'acctID';
const RETHINK_ORG_ID = 'ACCT27695401';

/*
    Roles
 */
const ROLE_PRIMARY_KEY = 'userDetailRec';
const ROLE_TABLE_NAME = 'userDetail';
const ROLE_USER_ID = 'userDetailUserID';
const ROLE_PRODUCT = 'userDetailSubType';
const ROLE_LEVEL = 'userDetailValue';

/*
    Users
 */
const USER_PRIMARY_KEY = 'userRec';
const USER_TABLE_NAME = 'users';
const USER_ACCOUNT_ID = 'userAcctID';
const USER_DASHBOARD_ID = 'userID';
const USER_EMAIL = 'userEmail';
const USER_FIRST_NAME = 'userFirstName';
const USER_LAST_NAME = 'userLastName';
const USER_PASSWORD = 'userPass';
const USER_USERNAME = 'userLogin';
