<?php

namespace RethinkLegacyDashboard\Providers;

use Illuminate\Support\ServiceProvider;

class DatabaseProvider extends ServiceProvider
{
    private $packageDir;

    /**
     * Bootstrap the application services.
     * 
     * @todo Refactor so that factories are pulled in automatically.
     */
    public function boot()
    {
        $this->packageDir = base_path('/vendor/rethink-group/legacy-dashboard/src/');
        $this->loadMigrationsFrom($this->packageDir.'database/migrations');

        // It would be nice if you didn't have to do this, but could use the 
        // above method so that the file structure would remain clean.
        $this->publishes([
            __DIR__.'/../database/factories/' => database_path('factories'),
        ], 'legacy-dashboard');
    }

    /**
     * Register the application services.
     */
    public function register()
    {
    }
}
