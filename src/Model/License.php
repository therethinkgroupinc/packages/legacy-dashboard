<?php

namespace RethinkLegacyDashboard\Model;

use DB;
use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    protected $primaryKey = LICENSE_PRIMARY_KEY;
    protected $table = LICENSE_TABLE_NAME;

    public $timestamps = false;
    protected $appends = ['price'];

    public function organization()
    {
        return $this->hasOne('DashboardOrganization', ORG_DASHBOARD_ID, 'organization_id');
    }

    public function getPriceAttribute()
    {
        $row = DB::table('store_auto_ship')
            ->where('sasItemID', $this->activeProdDetail)
            ->where('sasDateTimeCancel', '1900-01-01 00:00:00')
            ->orderByDesc('sasRec')
            ->first();

        return $this->attributes['price'] = (!empty($row)) ? (float) $row->sasPriceTotal : null;
    }
}
