<?php

namespace RethinkLegacyDashboard\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class DashboardUser extends Authenticatable
{
    use Notifiable;
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        // 'userPass'
    ];

    protected $primaryKey = USER_PRIMARY_KEY;
    protected $table = USER_TABLE_NAME;

    public $timestamps = false;

    public function organization()
    {
        return $this->hasOne('DashboardOrganization', ORG_DASHBOARD_ID, USER_ACCOUNT_ID);
    }

    public function licenses()
    {
        return $this->organization->licenses;
    }

    public function getIsRethinkAttribute()
    {
        return $this->account_id == RETHINK_ORG_ID;
    }

    /*
     The following attribute accessors and mutators are designed to allow us
     to change the reThink Dashboard columns at some point (as they are
     expected to) without breaking the API. However, this probably could
     be done using some sort of mapping function. - jhoward
     */
    public function getIdAttribute()
    {
        return $this->{USER_PRIMARY_KEY};
    }

    public function setIdAttribute($value)
    {
        $this->{USER_PRIMARY_KEY} = $value;
    }

    public function getUsernameAttribute()
    {
        return $this->{USER_USERNAME};
    }

    public function setUsernameAttribute($value)
    {
        $this->{USER_USERNAME} = $value;
    }

    public function getPasswordAttribute()
    {
        return $this->{USER_PASSWORD};
    }

    public function setPasswordAttribute($value)
    {
        $this->{USER_PASSWORD} = $value;
    }

    public function getEmailAttribute()
    {
        return $this->{USER_EMAIL};
    }

    public function setEmailAttribute($value)
    {
        $this->{USER_EMAIL} = $value;
    }

    public function getNameAttribute()
    {
        return $this->firstName.' '.$this->lastName;
    }

    public function getFirstNameAttribute()
    {
        return $this->{USER_FIRST_NAME};
    }

    public function setFirstNameAttribute($value)
    {
        $this->{USER_FIRST_NAME} = $value;
    }

    public function getLastNameAttribute()
    {
        return $this->{USER_LAST_NAME};
    }

    public function setLastNameAttribute($value)
    {
        $this->{USER_LAST_NAME} = $value;
    }

    public function getAccountIdAttribute()
    {
        return $this->{USER_ACCOUNT_ID};
    }

    public function setAccountIdAttribute($value)
    {
        $this->{USER_ACCOUNT_ID} = $value;
    }

    public function getDashboardIdAttribute()
    {
        return $this->{USER_DASHBOARD_ID};
    }

    public function setDashboardIdAttribute($value)
    {
        $this->{USER_DASHBOARD_ID} = $value;
    }

    public function getRememberToken()
    {
        return null; // not supported
    }

    public function setRememberToken($value)
    {
        // not supported
    }

    public function getRememberTokenName()
    {
        return null; // not supported
    }

    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute) {
            parent::setAttribute($key, $value);
        }
    }
}
