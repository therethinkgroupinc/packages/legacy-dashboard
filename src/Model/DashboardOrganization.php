<?php

namespace RethinkLegacyDashboard\Model;

use Illuminate\Database\Eloquent\Model;

class DashboardOrganization extends Model
{
    protected $primaryKey = ORG_PRIMARY_KEY;
    protected $table = ORG_TABLE_NAME;

    public $timestamps = false;

    public function getIsRethinkAttribute()
    {
        return $this->acctID == RETHINK_ORG_ID;
    }
}
